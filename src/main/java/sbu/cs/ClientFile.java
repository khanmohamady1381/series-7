package sbu.cs;

import java.io.*;
import java.net.Socket;

public class ClientFile {

    private Socket socket ;
    private int packetSize =1;
    private File file = null ;
    private String filePath ;
    private BufferedInputStream bis = null ;

    public ClientFile(Socket socket , String filePath) throws IOException {
        this.socket = socket;
        this.filePath = filePath ;
    }

    public int getPacketSize() {
        return packetSize;
    }


    public void sendFile () throws IOException, ClassNotFoundException {

        file = new File(filePath);
        double packetCounter=Math.ceil(((int) file.length())/ packetSize);
        System.out.println(packetCounter);
        bis = new BufferedInputStream(new FileInputStream(filePath));
        getFileAndSend(packetCounter);
        closeStreams();


    }

    private void getFileAndSend(double packetCounter) throws IOException {

        for(double i = 0; i< packetCounter; i++) {
            byte[] mybytearray = new byte[packetSize];
            bis.read(mybytearray, 0, mybytearray.length);
            System.out.println("Packet:"+(i+1));
            OutputStream os = socket.getOutputStream();
            os.write(mybytearray, 0,mybytearray.length);
            os.flush();
        }

    }
    private void closeSocket() throws IOException {
        socket.close();
    }

    public void closeStreams() throws IOException {
        bis.close();
    }


}


