package sbu.cs;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class MultiServer implements Runnable{

    private static final int HTTP_PORT = 12345;
    private final String filePath ;

    public MultiServer(String filePath) {
        this.filePath = filePath;
    }


    @Override
    public void run() {
        try {

            ServerSocket serversocket = new ServerSocket(HTTP_PORT);
            Socket socket = serversocket.accept();
            String fileName = getFileName(socket);
            ServerFile serverFile = new ServerFile(socket, fileName, filePath);
            serverFile.saveFile();
            serversocket.close();
            socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private static String getFileName(Socket socket) throws IOException {
        DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
        String fileName =  dataInputStream.readUTF();
        return fileName;
    }



}


