package sbu.cs;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    private static final int HTTP_PORT = 12345;
    private static final String LOCAL_HOSTS = "127.0.0.1";
    /**
     * Create a socket and connect to server then send fileName and after that send file to be saved on server side.
     *  in here filePath == fileName
     *
     * @param args a string array with one element
     * @throws IOException
     */

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        String filePath = args[0];      // "sbu.png" or "book.pdf"


        Socket s = new Socket(LOCAL_HOSTS,HTTP_PORT);
        DataOutputStream dataOutputStream = new DataOutputStream(s.getOutputStream());
        dataOutputStream.writeUTF(filePath);
        ClientFile clientFile = new ClientFile(s,filePath);
        clientFile.sendFile();
        dataOutputStream.close();
        s.close();


    }

}
