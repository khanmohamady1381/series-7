package sbu.cs;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    private static final int HTTP_PORT = 12345;
    /**
     * Create a server here and wait for connection to establish,
     *  then get file name and after that get file and save it in the given directory
     *
     * @param args an string array with one element
     * @throws IOException
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {

        // below is the name of directory which you must save the file in it
        String directory = args[0];     // default: "server-database"
        new MultiServer(directory).run();
    }

}
