package sbu.cs;

import java.io.*;
import java.net.Socket;

public class ServerFile {

    private Socket socket;
    private String filePath;
    private String fileName;
    private File file = null;
    private FileOutputStream fos = null;
    private BufferedOutputStream bos = null;

    public ServerFile(Socket socket, String fileName, String filePath) throws IOException {
        this.socket = socket;
        this.fileName = fileName;
        this.filePath = filePath;
    }


    public void saveFile() throws IOException {

        file = new File(filePath+"/"+fileName);
        fos = new FileOutputStream(file);
        // if here founds fileNotFoundException the only thing that you need to do is verify true filepath
        bos = new BufferedOutputStream(fos);
        double packetCounts = Math.ceil(8203108);
        System.out.print(packetCounts);
        getFileAndSave(packetCounts);
        closeConnection();
//        closeStreams();

    }

    private void getFileAndSave(double packetCounts) throws IOException {
        for (double i = 0; i < packetCounts + 1; i++) {
            InputStream is = socket.getInputStream();
//            byte[] mybytearray = new byte[packetsize];
            byte[] bytesRead = is.readAllBytes();
            System.out.println("Packet:" + (i + 1));
            bos.write(bytesRead);
        }
    }


    public void closeConnection() throws IOException {
        socket.close();
    }

    public void closeStreams() throws IOException {
        fos.close();
        bos.close();
    }
}

